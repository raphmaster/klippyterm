# klippyterm

a minimalist interactive terminal for klipper

launch klipper and open an interactive terminal
call launch command for each printer you want to start
each printer can run different version of klipper and have different configuration

each printer deserve its own directory.
in the printer directory, there is the "printer.cfg" file used by klipper
there is also the terminal "term.cfg" configuration file, klipper logs and some pid files
